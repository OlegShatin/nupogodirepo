﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace NuPogodi
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer mainTimer;
        Track[] tracks;
        Busket busket;
        List<Egg> fallenEggs;
        internal static readonly int SPEED = 300;
        State currentGameState;
        //enum for game state
        private enum State
        {
            Stopped, Paused, Runned
        }

        public MainWindow()
        {
            InitializeComponent();
            Canvas.SetLeft(startButton, (this.Width - startButton.Width) / 2);
            Canvas.SetTop(startButton, (this.Height - startButton.Height) / 2);
            currentGameState = State.Stopped;
        }
        //startButton click: lets start game
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            //startButton.Visibility = Visibility.Hidden;
            //startButton.IsEnabled = false;
            switch (currentGameState)
            {
                case State.Stopped:
                    //tracks initial
                    tracks = new Track[]
                    {
                        new Track(Pos.TL, gameCanvas, this),
                        new Track(Pos.TR, gameCanvas, this),
                        new Track(Pos.BL, gameCanvas, this),
                        new Track(Pos.BR, gameCanvas, this)
                    };

                    //busket initial - set busket position
                    if (busket == null)
                    {
                        busket = new Busket(Pos.TL, gameCanvas);
                    }

                    //timer initial
                    KeyDown += MainWindow_KeyDown;
                    mainTimer = new DispatcherTimer();
                    mainTimer.Tick += new EventHandler(timerTick);
                    mainTimer.Interval = new TimeSpan(0, 0, 0, 0, 10);
                    mainTimer.Start();
                    scoreLabel.Content = "0";
                    startButton.Content = "||";
                    currentGameState = State.Runned;
                    break;

                case State.Runned:
                    mainTimer.Stop();
                    startButton.Content = "Resume!";
                    currentGameState = State.Paused;
                    break;

                case State.Paused:
                    mainTimer.Start();
                    startButton.Content = "||";
                    currentGameState = State.Runned;
                    break;
            }

        }

        //main timer step
        private void timerTick(object sender, EventArgs e)
        {
            foreach (var track in tracks)
            {
                track.trackTick(busket);
            }
        }

        //if game is failed - all eggs fallen on floor
        internal void stopGame()
        {
            KeyDown -= MainWindow_KeyDown;
            mainTimer.Stop();
            fallenEggs = new List<Egg>();
            foreach (Track track in tracks)
            {
                track.giveEggs(fallenEggs);
            }
            foreach (Egg egg in fallenEggs)
            {
                egg.changeDirection((int)gameCanvas.ActualHeight);
            }
            mainTimer = new DispatcherTimer();
            mainTimer.Tick += new EventHandler(eggsFallTick);
            mainTimer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            mainTimer.Start();
        }
        private void eggsFallTick(object sender, EventArgs e)
        {
            Egg toRemoveEgg = null;
            foreach (Egg egg in fallenEggs)
            {
                if (!egg.IsOut)
                {
                    egg.move();
                }
                else
                {
                    gameCanvas.Children.Remove(egg.Body);
                    toRemoveEgg = egg;
                }
            }
            fallenEggs.Remove(toRemoveEgg);
            //when all eggs already fell
            if (fallenEggs.Count == 0)
            {
                mainTimer.Stop();
                startButton.Content = "Go?";
                currentGameState = State.Stopped;
            }
        }

        //keyDowns - change busket position
        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.E:
                    busket.Pos = Pos.TL;
                    break;
                case Key.D:
                    busket.Pos = Pos.BL;
                    break;
                case Key.O:
                    busket.Pos = Pos.TR;
                    break;
                case Key.K:
                    busket.Pos = Pos.BR;
                    break;

            }
        }

    }

    internal class Busket
    {
        private Canvas _gameCanvas;
        private Rectangle _rect;
        private Pos _position;
        //redraw on canvas
        public Pos Pos
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                switch (Pos)
                {
                    case Pos.TL:
                        Canvas.SetTop(_rect, _gameCanvas.ActualHeight / 3);
                        Canvas.SetLeft(_rect, _gameCanvas.ActualWidth / 3);
                        break;
                    case Pos.TR:
                        Canvas.SetLeft(_rect, _gameCanvas.ActualWidth * 2 / 3 - _rect.Width);
                        Canvas.SetTop(_rect, _gameCanvas.ActualHeight / 3);
                        break;
                    case Pos.BL:
                        Canvas.SetTop(_rect, _gameCanvas.ActualHeight * 2 / 3);
                        Canvas.SetLeft(_rect, _gameCanvas.ActualWidth / 3);
                        break;
                    case Pos.BR:
                        Canvas.SetLeft(_rect, _gameCanvas.ActualWidth * 2 / 3 - _rect.Width);
                        Canvas.SetTop(_rect, _gameCanvas.ActualHeight * 2 / 3);
                        break;
                }
            }

        }

        public Busket(Pos defaultPosition, Canvas gameCanvas)
        {
            _gameCanvas = gameCanvas;
            _rect = new Rectangle();
            _rect.Stroke = Brushes.Chocolate;
            _rect.StrokeThickness = 2;
            _rect.Fill = new SolidColorBrush(Colors.Black);
            _rect.Width = 20;
            _rect.Height = 20;
            _gameCanvas.Children.Add(_rect);
            this.Pos = defaultPosition;
        }
    }
    public class Track
    {
        public Pos Pos { get; private set; }
        public Line Line { get; private set; }
        private Queue<Egg> _eggs;
        private static Random rnd = new Random();
        private Canvas _gameCanvas;
        private MainWindow _currentWindow;

        public Track(Pos position, Canvas gameCanvas, MainWindow mainWindow)
        {
            _currentWindow = mainWindow;
            _gameCanvas = gameCanvas;
            this.Pos = position;
            Line = new Line();
            _eggs = new Queue<Egg>();
            Line.Stroke = Brushes.Chocolate;
            Line.StrokeThickness = 2;
            gameCanvas.Children.Add(Line);
            switch (Pos)
            {
                case Pos.TL:
                    Line.X1 = 0;
                    Line.Y1 = 0;
                    Line.X2 = gameCanvas.ActualWidth / 3;
                    Line.Y2 = gameCanvas.ActualHeight / 3;
                    break;
                case Pos.TR:
                    Line.X1 = gameCanvas.ActualWidth;
                    Line.Y1 = 0;
                    Line.X2 = gameCanvas.ActualWidth * 2 / 3;
                    Line.Y2 = gameCanvas.ActualHeight / 3;
                    break;
                case Pos.BL:
                    Line.X1 = 0;
                    Line.Y1 = gameCanvas.ActualHeight / 3;
                    Line.X2 = gameCanvas.ActualWidth / 3;
                    Line.Y2 = gameCanvas.ActualHeight * 2 / 3;
                    break;
                case Pos.BR:
                    Line.X1 = gameCanvas.ActualWidth;
                    Line.Y1 = gameCanvas.ActualHeight / 3;
                    Line.X2 = gameCanvas.ActualWidth * 2 / 3;
                    Line.Y2 = gameCanvas.ActualHeight * 2 / 3;
                    break;


            }
        }

        internal void trackTick(Busket busket)
        {
            //new eggs
            if (rnd.NextDouble() < 0.002)
            {
                Egg newEgg = new Egg(Line);
                _gameCanvas.Children.Add(newEgg.Body);
                _eggs.Enqueue(newEgg);
            }

            //moving of eggs on this track
            foreach (Egg egg in _eggs)
            {
                if (!egg.IsOut)
                    egg.move();
            }
            if (_eggs.Count != 0 && _eggs.Peek().IsOut)
            {
                if (busket.Pos == this.Pos)
                {
                    var egg = _eggs.Dequeue();
                    _gameCanvas.Children.Remove(egg.Body);
                    _currentWindow.scoreLabel.Content = Convert.ToString(int.Parse((string)_currentWindow.scoreLabel.Content) + 1);
                }
                else
                {
                    _currentWindow.stopGame();
                }
            }
        }

        internal void giveEggs(List<Egg> fallenEggs)
        {
            while (_eggs.Count != 0)
            {
                fallenEggs.Add(_eggs.Dequeue());
            }
        }
    }

    //enum for postions of tracks and busket: TL - top-left, TR - top-right, BL- bottom-left
    //BR - bottom-right
    public enum Pos { TL, TR, BL, BR }
    //egg
    public class Egg
    {
        public Ellipse Body { get; private set; }
        private Vector _coordinates;
        private Vector _direction;
        private double _endOfTrack;
        public Boolean IsOut { get; private set; }

        //egg init
        public Egg(Line line)
        {
            Body = new Ellipse();
            Body.Fill = new SolidColorBrush(Colors.White);
            Body.Width = 15;
            Body.Height = 15;
            if (line.X2 > line.X1)
            {
                _coordinates = new Vector(line.X1 + Body.Width, line.Y1 - 5);
            }
            else
            {
                _coordinates = new Vector(line.X1 - Body.Width, line.Y1 - Body.Height);
            }
            Canvas.SetTop(Body, _coordinates.Y);
            Canvas.SetLeft(Body, _coordinates.X);
            _direction = new Vector((line.X2 - line.X1) / MainWindow.SPEED, (line.Y2 - line.Y1) / MainWindow.SPEED);
            _endOfTrack = line.Y2 - Body.Height;
        }

        //move process
        public void move()
        {
            Canvas.SetTop(Body, _coordinates.Y);
            Canvas.SetLeft(Body, _coordinates.X);
            _coordinates += _direction;
            IsOut = _coordinates.Y > _endOfTrack;
        }
        public void changeDirection(int floor)
        {
            IsOut = false;
            _direction = new Vector(0, 1);
            _endOfTrack = floor;
        }
    }
}
